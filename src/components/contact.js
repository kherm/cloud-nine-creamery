import React from 'react'
import styled from 'styled-components';
import manyFlavors from "../images/manyFlavors.jpg";
import logo from "../images/logo2-black.png"
import { theme, TextDiv, Form } from './globalStyles'
import Button from './button'

const Wrapper = styled.div`
   display: grid;
   grid-template-columns: 1fr 2fr;
   grid-gap: 10px;
   background-color: ${theme.greyBlue};
   background-image: url(${manyFlavors});
   background-position: left;
   background-size: cover;
   background-repeat: no-repeat;
   padding: 7rem 4rem;
   @media (max-width: 750px) {
      grid-template-columns: 1fr;
      padding: 7rem 1rem;
      background-position: center;
   }
   h4 {
      line-height: 1.5;
      span {
         padding: 0;
      }
   }
`;

const LeftSide = styled.div`
   min-height: 150px;
   display: grid;
   justify-items: center;
   align-items: center;
   @media (max-width: 750px) {
      display: none;
   }
`;

const RightSide = TextDiv;

class Contact extends React.Component {
   state = {
      name: '',
      email: '',
      message: ''
   }

   saveToState = (e) => {
      this.setState({ [e.target.name]: e.target.value });
   }

   handleSubmit = (e) => {
      fetch("/", {
         method: "POST",
         headers: { "Content-Type": "application/x-www-form-urlencoded" },
         body: encode({ "form-name": "contact", ...this.state })
      })
         .catch(error => alert(error));

      e.preventDefault();
   }

   render() {
      return (
         <Wrapper id="contact">
            <LeftSide data-aos="flip-right">
               <img src={logo} alt="logo" />
            </LeftSide>
            <RightSide data-aos="flip-left" backgroundColor={`rgba(250,250,250,0.65)`}>
               <h1>Get in Touch</h1>
               <h4>
                  Got questions or comments? We're happy to hear from you, so drop us a
                  line by filling out the form below! &nbsp;
                  {/* <span role="img" aria-label="pointing-down">👇</span> */}
                  <span role="img" aria-label="pencil">✏️</span>
               </h4>
               <Form
                  method="post"
                  name="contact"
                  data-netlify="true"
                  action="/success"
                  onSubmit={this.handleSubmit}
               >
                  <input type="hidden" name="form-name" value="contact" />
                  <input
                     type="text"
                     name="name"
                     placeholder="Name"
                     value={this.state.name}
                     onChange={this.saveToState}
                     required
                  />
                  <input
                     type="text"
                     name="email"
                     placeholder="Email"
                     value={this.state.email}
                     onChange={this.saveToState}
                     required
                  />
                  <textarea
                     rows="5"
                     name="message"
                     placeholder="Message"
                     value={this.state.message}
                     onChange={this.saveToState}
                  />
                  <Button type="submit">Send</Button>
               </Form>
            </RightSide>
         </Wrapper >
      );
   }
}

export default Contact;