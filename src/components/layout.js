/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'
import Nav from "./nav"
import Footer from "./footer"
import "./layout.css"

const Div = styled.div`
  overflow-x: hidden;
`;

const Layout = ({ children }) => {

  return (
    <Div>
      <Nav />
      <main>{children}</main>
      <Footer />
    </Div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
