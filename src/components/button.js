import React from 'react'
import styled from 'styled-components';

const defaultColour = "#405c78"; //steel blue

const ButtonStyle = styled.button`
   padding: 0.75rem;
   border: 2px solid ${props => props.colour || defaultColour};
   border-radius: 20px;
   background-color: transparent;
   box-shadow: 2px 2px grey;
   letter-spacing: 1px;
   color: ${props => props.colour || defaultColour};
   font-weight: bolder;
   cursor: pointer;
   &:hover {
      background-color: ${props => props.colour || defaultColour};
      color: white;
   }
   `;

const Button = ({ colour, type, onClick, children }) => (
   <ButtonStyle
      colour={colour}
      type={type}
      onClick={onClick}
   >
      {children}
   </ButtonStyle>
);

export default Button
