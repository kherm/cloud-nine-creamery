import styled from 'styled-components'
import React from "react"
import logo from "../images/logo-icon.png";
import { theme, keyframe } from './globalStyles';

const Wrapper = styled.div`
  background-color: black;
  height: 100vh;
  display: grid;
  grid-template-rows: 1fr 4fr 1fr;
  justify-items: center;

  * {
    margin: 0;
  }
  img {
    animation: ${keyframe.fadeIn} 2.5s;
    align-self: end;
    max-height: 100%;
  }
`;

const Div = styled.div`
  height: 25vh;
`;

const TagLine = styled.h1`
  color: white;
  align-self: center;
  padding-bottom: 1rem;
  font-family: ${theme.headlineFont};
  letter-spacing: 3px;
  text-align: center;
`;

const Cover = () => (
  <Wrapper>
    <Div />
    <img src={logo} alt="logo" />
    <TagLine>Handmade in Terrace, BC</TagLine> */
  </Wrapper>
)

export default Cover;
