import React from 'react'
import styled from 'styled-components';
import { navigate } from 'gatsby';
import spoon from "../images/spoon.jpg";
import pinpoint from "../images/map-pin-marked.png"
import { theme, TextDiv } from './globalStyles'
import Button from './button';

const Wrapper = styled.div`
   display: grid;
   grid-template-columns: 1fr 1fr;
   grid-gap: 10px;
   background-color: ${theme.greyBlue};
   background-image: url(${spoon});
   background-position: left;
   background-size: auto 100%;
   background-repeat: no-repeat;
   padding: 7rem 4rem;
   @media (max-width: 750px) {
      grid-template-columns: 1fr;
      padding: 7rem 1rem;
   }


   button {
      margin-bottom: 1rem;
   }
   p {
      margin: 0;
      span {
         padding: 0;
      }
   }
   #pinpoint {
      width: 20px;
      margin: 0;
      padding: 0;
   }
`;

const LeftSide = styled.div`
   min-height: 350px;
`;

const RightSide = TextDiv;

const CenterP = styled.p`
   text-align: center;
   margin: 0;
`;

const Bold = styled.span`
   font-weight: bold;
`;

const A = styled.a`
   padding: 1rem 0;
   text-decoration: none;
   color: ${theme.caramel};
   font-weight: bold;
`;

const Buy = () => (
   <Wrapper id="buy">
      <LeftSide />
      <RightSide data-aos="zoom-in">
         <h1>Get Your Ice Cream!</h1>
         <p>
            All our ice creams are hand-crafted and made with seasonal local ingredients.
            We even have non dairy options available
            <span role="img" aria-label="smiley-face">😊</span>
            <span role="img" aria-label="coconut">🥥</span>.
            Join the <Bold>Cloud Nine Pint Club</Bold> to get monthly pints of surprise
            flavours delivered right to your door!
         </p>
         <CenterP>
            <Button colour={theme.caramel} onClick={e => navigate("/pintclub/")}>
               Join the Pint Club
            </Button>
            <br />
            OR
         </CenterP>
         <p>
            Pick up a pint today at Terrace's local bakery. A variety of flavours are
            stocked every week!
         </p>
         <CenterP>
            <A href="https://www.google.com/maps/place/Baker+Extraordinaire/@54.5185188,-128.5926887,17z/data=!3m1!4b1!4m5!3m4!1s0x547499ee61615dbf:0xe9081d58df558305!8m2!3d54.5185157!4d-128.5905001"
               target="_blank"
               rel="noopener noreferrer"
            >
               <img src={pinpoint} alt="pinpoint" id="pinpoint" /> Baker's Extraordinaire<br />
               4630 Park Avenue<br />
               Terrace, BC, V8G 1V7<br />
            </A>
         </CenterP>
      </RightSide>
   </Wrapper >
);

export default Buy;

