import React from 'react'
import styled from 'styled-components';
import Image from './image'
import blackberries from "../images/blackberries.jpg";
import { TextDiv } from './globalStyles'

const Wrapper = styled.div`
   display: grid;
   grid-template-columns: 1fr 1fr;
   grid-gap: 10px;
   background-image: url(${blackberries});
   background-position: center;
   background-size: cover;
   padding: 7rem 4rem;
   @media (max-width: 750px) {
      grid-template-columns: 1fr;
      padding: 7rem 1rem;
   }
`;

const LeftSide = TextDiv;

const RightSide = styled.div`
   display: grid;
   align-items: center;
   img {
      margin: 0;
      border-radius: 100px;
   }
`;

const About = () => (
   <Wrapper id="about">
      <LeftSide data-aos="fade-right">
         <h1>About Us</h1>
         <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            It has survived not only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged.
         </p>
      </LeftSide>
      <RightSide data-aos="fade-left">
         <Image pic="owners" />
      </RightSide>
   </Wrapper>
);

export default About;

