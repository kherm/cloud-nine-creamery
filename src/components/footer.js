import React from 'react';
import styled from 'styled-components'
import { theme } from './globalStyles'
import facebook from '../images/facebook-logo-button.png'
import instagram from '../images/instagram-logo.png';

const Wrapper = styled.footer`
   display: grid;
   justify-items: center;
   align-items: center;
   min-height: 100px;
   background-color: black;
   color: white;
   font-family: ${theme.headlineFont};
   padding: 1rem;
   h2 {
      letter-spacing: 2px;
   }
   h3 {
      text-align: center;
      line-height: 1.6;
   }
`;

const ImageWrapper = styled.div`
   display: flex;
   img {
      max-width: 50px;
      margin: 0.75rem;
   }
`;

const Footer = () => (
   <Wrapper>
      <h3>Get the scoop on the latest flavors! <br />
         <span role="img" aria-label="ice-cream">🍨</span>
         <span role="img" aria-label="arrow">➡️</span>
         <span role="img" aria-label="yummy-face">😋</span>
      </h3>
      <h2>Follow Us!</h2>
      <ImageWrapper>
         <a href="https://www.facebook.com/CloudNineCreameryTBC/" target="_blank" rel="noopener noreferrer">
            <img src={facebook} alt="facebook-link" />
         </a>
         <a href="https://www.instagram.com/cloudninecreamery/" target="_blank" rel="noopener noreferrer">
            <img src={instagram} alt="instagram-link" />
         </a>
      </ImageWrapper>
      Cloud Nine Creamery © {new Date().getFullYear()}
   </Wrapper >
);

export default Footer;