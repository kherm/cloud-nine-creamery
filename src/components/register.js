import React, { Component } from 'react'
import styled from 'styled-components'
import fourPack from '../images/fourPack.jpg'
import fourPackCover from '../images/fourPackCover.jpg'
import { Form } from './globalStyles'
import Button from './button'

const PintsImage = styled.div`
  background-image: url(${fourPack});
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
  height: 400px;
  &:hover {
    background-image: url(${fourPackCover});
  }
`;

const Wrapper = styled.div`
  margin-top: 2rem;
  h4 {
    margin: 0;
    padding-bottom: 0.5rem;
  }
  .policy {
    font-size: 0.8rem;
    padding-top: 0;
    line-height: 1.3;
  }
`;

const RadioButtons = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 0;
  h4 {
    grid-column: 1 / -1;
  }
  input {
    margin-right: 1rem;
  }
  @media (max-width: 575px) {
    grid-template-columns: 1fr;
    label {
      padding: 0.5rem;
    }
  }
`;

class Registration extends Component {
  state = {
    name: '',
    email: '',
    address: '',
    phone: '',
    shipment: 'pickup',
    preference: 'noPreference',
    subscription: '2x3mo'
  }

  saveToState = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    return (
      <Wrapper id="registration">
        <h1>Registration</h1>
        <Form
          method="post"
          name="register"
          data-netlify="true"
        >
          <p>
            If this is a gift, please provide your name and email
            in the comments section below.
          </p>
          <input type="hidden" name="form-name" value="register" />
          <input
            type="text"
            name="name"
            placeholder="Name"
            value={this.state.name}
            onChange={this.saveToState}
            required
          />
          <input
            type="text"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.saveToState}
            required
          />
          {/* todo: address validation */}
          <input
            type="text"
            name="address"
            placeholder="Address"
            value={this.state.address}
            onChange={this.saveToState}
            required
          />
          <input
            type="text"
            name="phone"
            placeholder="Phone Number"
            value={this.state.phone}
            onChange={this.saveToState}
            required
          />

          <RadioButtons>
            <h4>Shipment Method: </h4>
            <label>
              <input
                type="radio"
                name="shipment"
                value="pickup"
                onChange={this.saveToState}
                checked={this.state.shipment === 'pickup'}
              />
              Pick-up
            </label>
            <label>
              <input
                type="radio"
                name="shipment"
                value="delivery"
                onChange={this.saveToState}
                checked={this.state.shipment === 'delivery'}
              />
              Delivery
            </label>

            <h4>Ice Cream Preferences: </h4>
            <label>
              <input
                type="radio"
                name="preference"
                value="noPreference"
                onChange={this.saveToState}
                checked={this.state.preference === 'noPreference'}
              />
              No Preference
            </label>
            <label>
              <input
                type="radio"
                name="preference"
                value="dairyOnly"
                onChange={this.saveToState}
                checked={this.state.preference === 'dairyOnly'}
              />
              Dairy Only
            </label>
            <label>
              <input
                type="radio"
                name="preference"
                value="nonDairyOnly"
                onChange={this.saveToState}
                checked={this.state.preference === 'nonDairyOnly'}
              />
              Non-Dairy Only
            </label>
            <label>
              <input
                type="radio"
                name="preference"
                value="halfAndHalf"
                onChange={this.saveToState}
                checked={this.state.preference === 'halfAndHalf'}
              />
              Half and Half
            </label>

            <h4>Subscription Type: </h4>
            <label>
              <input
                type="radio"
                name="subscription"
                value="2x3mo"
                onChange={this.saveToState}
                checked={this.state.subscription === '2x3mo'}
              />
              2 pints per month x 3 months = $78
            </label>
            <label>
              <input
                type="radio"
                name="subscription"
                value="2x6mo"
                onChange={this.saveToState}
                checked={this.state.subscription === '2x6mo'}
              />
              2 pints per month x 6 months = $144
            </label>
            <label>
              <input
                type="radio"
                name="subscription"
                value="3x3mo"
                onChange={this.saveToState}
                checked={this.state.subscription === '3x3mo'}
              />
              3 pints per month x 3 months = $111
            </label>
            <label>
              <input
                type="radio"
                name="subscription"
                value="3x6mo"
                onChange={this.saveToState}
                checked={this.state.subscription === '3x6mo'}
              />
              3 pints per month x 6 months = $210
            </label>

          </RadioButtons>

          <textarea
            rows="3"
            name="comment"
            placeholder="Additional Information or Comments"
            value={this.state.comment}
            onChange={this.saveToState}
          />

          <h4>Delivery Policy</h4>
          <p className="policy">
            Someone must be home to receive Pint Club Delivery. If order is
            not received, it can be picked up or re-delivered with additional
            Delivery Fee Charges. If you need to change your start month,
            please contact us.
          </p>

          <Button type="submit">Sign Me Up!</Button>
        </Form>
        <PintsImage />
      </Wrapper>
    )
  }
}

export default Registration;