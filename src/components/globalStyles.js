import styled, { keyframes } from 'styled-components';

const theme = {
   headlineFont: `'Gayathri', sans-serif;`,
   beige: `#c6bfac`,
   caramel: `#3d251b`,
   greyBlue: `#dadbdd`,
};

const TextDiv = styled.div`
   background-color: ${props => props.backgroundColor || theme.beige};
   border-radius: 100px;
   font-family: Arial, Helvetica, sans-serif;
   padding: 5%;
   * {
      padding: 1rem;
   }
   h1 {
      padding-top: 2rem;
      text-align: center;
      margin: 0;
   }
`;

const fadeInKeyframe = keyframes`
   from { opacity: 0; }
   to { opacity: 1; }
`;

const keyframe = {
   fadeIn: fadeInKeyframe,
};

const Form = styled.form`
   display: grid;
   justify-content: stretch;
   grid-gap: 12px;
   input, textarea {
      font-size: 1.2rem;
      padding: 1rem;
      border: none;
      border-bottom: 3px solid grey;
      border-radius: 10px;
   }
`;

export {
   theme,
   keyframe,
   TextDiv,
   Form
};