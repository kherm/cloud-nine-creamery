import { Link, navigate } from "gatsby"
import styled from 'styled-components'
import React, { Component } from "react"
import name from "../images/logo-name.png"
import { theme } from './globalStyles';

const Menu = styled.nav`
   z-index: 1;
   width: 100%;
   height: 25%;
   background-color: black;
   display: grid;
   justify-items: center;
   align-items: center;
   position: fixed;
   transition: 0.5s;
   &.smaller {
      height: 15%;
      font-size: 0.8rem;
   }
   img {
      margin: 0;
      height: 80%;
      cursor: pointer;
   }
   ul {
      display: flex;
      justify-content: space-evenly;
      margin: 0;
      margin-bottom: 0.5rem;
      padding: 0;
      list-style-position: inside;
      @media (max-width: 330px) {
         list-style-type: none;
      }
   }
`;

const Li = styled.li`
   a {
      color: white;
      text-decoration: none;
      display: inline-block;
      cursor: pointer;

      &:after {
         display: block;
         content: '';
         width: 0;
         height: 2px;
         background: white;
         transition: width 0.4s;
      }
      &:hover::after {
         width: 100%;
      }
   }

   color: white;
   margin: 0 1rem;
   font-family: ${theme.headlineFont};
`;

export default class Nav extends Component {
   componentDidMount() {
      window.addEventListener("scroll", this.resizeNavOnScroll);
   }

   componentWillUnmount() {
      window.removeEventListener("scroll", this.resizeNavOnScroll);
   }

   resizeNavOnScroll = () => {
      const distanceY = window.pageYOffset || document.documentElement.scrollTop;
      const shrinkOn = 200;
      const navBar = document.getElementById("navBar");

      if (distanceY > shrinkOn) {
         navBar.classList.add("smaller");
      } else {
         navBar.classList.remove("smaller");
      }
   }

   render() {
      return (
         <Menu id="navBar">
            <img src={name} alt="company name" onClick={
               e => navigate('/')
            } />
            <ul>
               <Li><Link to="/#about">About Us</Link></Li>
               <Li><Link to="/#buy">Purchase</Link></Li>
               <Li><Link to="/#contact">Contact</Link></Li>
            </ul>
         </Menu>
      );
   }
}