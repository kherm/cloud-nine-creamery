import React from "react"
import AOS from 'aos';
import 'aos/dist/aos.css';
import SEO from "../components/seo"
import Layout from "../components/layout"
import Cover from '../components/cover';
import About from "../components/about";
import Buy from "../components/Buy";
import Contact from "../components/contact";

class IndexPage extends React.Component {
  componentDidMount() {
    this.aos = AOS
    this.aos.init();
  }
  componentDidUpdate() {
    this.aos.refresh();
  }
  render() {
    return (
      <Layout>
        <SEO title="Home" />
        <Cover />
        <About />
        <Buy />
        <Contact />
      </Layout>
    );
  }
}

export default IndexPage;
