import React from "react"
import { Link } from "gatsby"
import styled from 'styled-components'
import Layout from "../components/layout"
import SEO from "../components/seo"
import surprise from '../images/surprise.jpg'
import iceCreamBowl from '../images/iceCreamBowl.jpg'
import { TextDiv, theme } from '../components/globalStyles'
import Registration from '../components/register'

const Div = styled.div`
  height: 25vh;
`;

const Wrapper = styled.div`
  display: grid;
  justify-items: center;
  background-image: url(${surprise});
  background-size: contain;

  .content {
    display: grid;
    justify-items: center;
    max-width: 70%;
    margin: 1rem;
    span {
      padding: 2px ;
    }
    p, ul, li {
      margin: 0;
    }
    li {
      padding: 0.2rem;
    }
    ul {
      list-style-position: inside;
    }
    #link {
      display: inline-block;
      text-decoration: none;
      color: ${theme.caramel};
      cursor: pointer;
      &:after {
         display: block;
         content: '';
         width: 0;
         height: 2px;
         background: ${theme.caramel};
         transition: width 0.4s;
      }
      &:hover::after {
         width: 100%;
      }
      h3 {
        padding: 0.5rem;
        margin: 0;
      }
    }
    @media (max-width: 750px) {
      max-width: 90%;
    }
  }
`;

const Content = TextDiv;

const PintClub = () => {
  return (
    <Layout>
      <SEO title="Pint Club Subscription Sign up" />

      <Wrapper>
        <Div />
        <Content backgroundColor={`rgba(250,250,250,0.85)`} className="content">
          <h1>Join the Pint Club!</h1>
          <p>
            Subscribe and enjoy hand-crafted artisan ice cream
          <span role="img" aria-label="ice-cream">🍧</span>
            using fresh local ingredients
          <span role="img" aria-label="cherry">🍒</span>
            delivered right to your door!
        </p>
          <ul>
            <li>
              Delivery to subscribers will on the 3rd Tuesday of every month <br />
              (within Terrace city limits).
            <span role="img" aria-label="bike">🚴</span>
            </li>
            <li>
              Pick up option is also available. Just let us know!
            <span role="img" aria-label="smiley-face">😊</span>
            </li>
            <li>
              Please register and pay by the 2nd Friday of the month to receive
              your ice cream for the next delivery date!
            <span role="img" aria-label="gift">🎁</span>
            </li>
          </ul>

          <Link to="/pintclub/#registration" id="link">
            <h3>Register Here <span role="img" aria-label="point-down">👇</span></h3>
          </Link>

          <img src={iceCreamBowl} alt="ice cream bowl" />

          <Registration />

        </Content>
      </Wrapper>
    </Layout >
  );
}

export default PintClub
