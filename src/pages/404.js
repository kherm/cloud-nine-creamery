import React from "react"
import { navigate } from "gatsby"
import styled from 'styled-components'
import Layout from "../components/layout"
import SEO from "../components/seo"
import Button from "../components/button"

const Div = styled.div`
  height: 25vh;
`;

const Wrapper = styled.div`
  height: 45vh;
  padding: 1rem;
  display: grid;
  justify-content: center;
  align-content: center;
  * {
    text-align: center;
  }
`;

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <Div />
    <Wrapper>
      <h1>Oops!</h1>
      <p>
        You just hit a page that doesn&#39;t exist :(
      </p>
      <Button onClick={e => navigate("/")}>
        Back Home
      </Button>
    </Wrapper>
  </Layout>
)

export default NotFoundPage
